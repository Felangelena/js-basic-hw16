"use strict";

let f0, f1, n;

f0 = parseInt(prompt("Please enter the first number of sequence", ""));
f1 = parseInt(prompt("Please enter the second number of sequence", ""));
n = parseInt(prompt("Please enter n number of sequence", ""));

let result = 0;

if (n === 1) {
    result = f0
} else if (n === 2) {
    result = f1
} else {
    for (let i = 2; i <= n; i++) {
    result = f0 + f1;
    f0 = f1;
    f1 = result;
}}

console.log(result);